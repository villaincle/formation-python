"""Example of using the dictionnary in python: swapping letters

Made by Atilla
"""

def swap(string):
	"""Swap the a with the e and the c with the n.
	"""

	dic = 	{	
		'a': 'e',
		'e': 'a',
		'c': 'n',
		'n': 'c'
		}

	res = ""
	for c in string:
		if c in dic:
			res += dic[c]
		else:
			res += c

	return res
		

if __name__ == '__main__':
	message = "Ceci est un message non crypte !"
	print message
	print swap(message)
	print swap(swap(message))
