"""Primaly file as python example.

Made by Atilla.
"""


import math

def is_prime(x, prime_list):
	"""Returns True if x is a prime number.
	To do the primal test we use a prime_list which MUST contains all the
	prime number < x.
	"""

	root = math.sqrt(x)
	for prime in prime_list:
		if x % prime == 0:  # x is a prime multiple
			return False
		if prime > root: # We do not need to go further
			break
	return True

def search_new_prime(prime_list):
	"""Returns the next prime number of the prime_list.
	We assume that the prime_list already contains 2 and 3.
	"""

	assert(2 in prime_list and 3 in prime_list)  # Do not trust anyone !

	last = prime_list[-1] + 2
	while(not is_prime(last, prime_list)):
		last += 2
	return last

def print_primes(n):
	"""Print the n first prime numbers.
	We assume that n >= 2.
	"""

	assert(n >= 2)
	prime_list = [2, 3]
	
	while(len(prime_list) < n):
		prime_list.append(search_new_prime(prime_list))

	print prime_list


if __name__ == '__main__':
	print_primes(1000)

